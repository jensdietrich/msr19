package test.nz.ac.vuw.ecs.msr19;

import nz.ac.vuw.ecs.msr19.ComputeAlphas;
import org.junit.Test;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static nz.ac.vuw.ecs.msr19.Commons.parsePLTags;
import static nz.ac.vuw.ecs.msr19.Commons.removeVersionInfo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * unit tests
 * @author jens dietrich
 */

public class TestComputeAlpha {

    @Test
    public void test1() throws Exception {
        File dir = new File("testdata/test1");
        assert dir.exists();
        Map<Integer,Integer> matches = ComputeAlphas.computeAlphas(dir);

        assertEquals(1,(int)matches.get(1));  // record 1 matches
        assertEquals(2,(int)matches.get(2)); // records 2 and 3 match
        assertNull(matches.get(3)); // no record matches
        assertEquals(1,(int)matches.get(4)); // record 24 matches
    }

}
