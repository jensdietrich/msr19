package test.nz.ac.vuw.ecs.msr19;

import nz.ac.vuw.ecs.msr19.Commons;
import nz.ac.vuw.ecs.msr19.PLTag;
import org.junit.Test;
import java.io.File;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static org.junit.Assert.assertEquals;

/**
 * unit tests
 * @author jens dietrich
 */
public class TestComputeCooccurence {

    @Test
    public void testLinguistTags() throws Exception {
        File file = new File("testdata/test2/LinguistTags.csv");
        Map<Integer, List<PLTag>> tags = Commons.importLinguistTags (file);
        Map<Commons.Pair,Integer> stats = Commons.computePairCounts(tags);

        assertEquals(2,(int)stats.get(new Commons.Pair(PLTag.java_,PLTag.sql)));
        assertEquals(2,(int)stats.get(new Commons.Pair(PLTag.java_,PLTag.python)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.c,PLTag.java_)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.c,PLTag.sql)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.python,PLTag.sql)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.csharp,PLTag.java_)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.csharp,PLTag.python)));

        assertEquals(7,stats.size());
    }

    @Test
    public void testUserTags() throws Exception {
        File file = new File("testdata/test2/UserTags.csv");
        Map<Integer, EnumSet<PLTag>> tags = Commons.importUserPLTags(file);
        Map<Commons.Pair,Integer> stats = Commons.computePairCounts(tags);

        assertEquals(2,(int)stats.get(new Commons.Pair(PLTag.java_,PLTag.sql)));
        assertEquals(2,(int)stats.get(new Commons.Pair(PLTag.java_,PLTag.python)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.c,PLTag.java_)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.c,PLTag.sql)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.python,PLTag.sql)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.csharp,PLTag.java_)));
        assertEquals(1,(int)stats.get(new Commons.Pair(PLTag.csharp,PLTag.python)));

        assertEquals(7,stats.size());
    }

}
