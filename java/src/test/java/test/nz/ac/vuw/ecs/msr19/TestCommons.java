package test.nz.ac.vuw.ecs.msr19;

import nz.ac.vuw.ecs.msr19.Commons;
import nz.ac.vuw.ecs.msr19.PLTag;
import org.junit.Test;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static nz.ac.vuw.ecs.msr19.Commons.*;

/**
 * unit tests
 * @author jens dietrich
 */

public class TestCommons {

    @Test
    public void testRemoveVersion1() throws Exception {
        assertEquals("python",removeVersionInfo("python"));
    }

    @Test
    public void testRemoveVersion2() throws Exception {
        assertEquals("python",removeVersionInfo("python-3.1"));
    }

    @Test
    public void testRemoveVersion3() throws Exception {
        assertEquals("python",removeVersionInfo("python-3.1beta"));
    }

    @Test
    public void testRemoveVersion4() throws Exception {
        assertEquals("objective-c",removeVersionInfo("objective-c"));
    }

    @Test
    public void testRemoveVersion5() throws Exception {
        assertEquals("objective-c",removeVersionInfo("objective-c-2.0"));
    }

    @Test
    public void testParseList1() throws Exception {
        String def = "<python-3.1><ruby><java-8><objective-c><brainfuck>";
        assertEquals(EnumSet.of(PLTag.python,PLTag.ruby,PLTag.java_,PLTag.objective_c),parsePLTags(def));
    }

    @Test
    public void testPairFinder1() throws Exception {
        List<PLTag> input = Stream.of(PLTag.java_,PLTag.csharp,PLTag.python).collect(Collectors.toList());
        Collection<Pair> pairs = Commons.findAllPairs(input,3);
        assertEquals(3,pairs.size());
        assertTrue(pairs.contains(new Pair(PLTag.csharp,PLTag.java_)));
        assertTrue(pairs.contains(new Pair(PLTag.csharp,PLTag.python)));
        assertTrue(pairs.contains(new Pair(PLTag.java_,PLTag.python)));
    }

    @Test
    public void testPairFinder2() throws Exception {
        List<PLTag>  input = Stream.of(PLTag.java_,PLTag.csharp,PLTag.python).collect(Collectors.toList());
        Collection<Pair> pairs = Commons.findAllPairs(input,2);
        assertEquals(1,pairs.size());
        assertTrue(pairs.contains(new Pair(PLTag.csharp,PLTag.java_)));
    }

    @Test
    public void testPairFinder3() throws Exception {
        List<PLTag> input = Stream.of(PLTag.java_,PLTag.csharp,PLTag.python).collect(Collectors.toList());
        Collection<Pair> pairs = Commons.findAllPairs(input,1000);
        assertEquals(3,pairs.size());
        assertTrue(pairs.contains(new Pair(PLTag.csharp,PLTag.java_)));
        assertTrue(pairs.contains(new Pair(PLTag.csharp,PLTag.python)));
        assertTrue(pairs.contains(new Pair(PLTag.java_,PLTag.python)));
    }

    @Test
    public void testPairFinder4() throws Exception {
        List<PLTag> input = Stream.of(PLTag.java_,PLTag.csharp,PLTag.python,PLTag.perl).collect(Collectors.toList());
        Collection<Pair> pairs = Commons.findAllPairs(input,3);
        assertEquals(3,pairs.size());
        assertTrue(pairs.contains(new Pair(PLTag.csharp,PLTag.java_)));
        assertTrue(pairs.contains(new Pair(PLTag.csharp,PLTag.python)));
        assertTrue(pairs.contains(new Pair(PLTag.java_,PLTag.python)));
    }

    private Set<String> setOf(String... tags) {
        return Stream.of(tags).collect(Collectors.toSet());
    }
}
