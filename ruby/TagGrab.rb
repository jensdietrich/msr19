require "dbi"
#Loads the database tags.  Then saves it to Min_Max_tags.csv files

 #Connect to the sql server
def connect()
	# connect to the MySQL server
	$dbh = DBI.connect("DBI:Mysql:sotorrent18_12:localhost", "root", "bokker")
	# get server version string and display it
	row = $dbh.select_one("SELECT VERSION()")
	puts "Server version: " + row[0]
end

#Get sample between the min and max value
def get_range_sample(min,max)
	posts = []
	sth	= $dbh.prepare("SELECT DISTINCT Id FROM Posts WHERE Id >= #{min} AND Id <  #{max} AND (PostTypeId = 1 OR PostTypeId = 2)")
	sth.execute
	# Copy the result to the array
	while row=sth.fetch do
		posts << row[0]
	end
	# Close the statement handle when done
	sth.finish

	return posts
end


#Get the id of the lastest snippet
def self.get_code_ids(sample_id)

	id = []

	sth = $dbh.prepare("SELECT Id FROM PostBlockVersion WHERE PostHistoryId = (SELECT MAX(PostHistoryId) FROM PostBlockVersion WHERE PostId= #{sample_id}) AND PostBlockTypeId = 2")
	sth.execute
	# Copy the result to the array
	while row=sth.fetch do
		id << row[0]
	end

	# Close the statement handle when done
	sth.finish

	return id
end

#Gets the tags from the database
def get_database_tags(sample_id)
	ans = nil;
	sth = $dbh.prepare("SELECT ParentId FROM Posts WHERE Id = #{sample_id}")
	sth.execute
	row = sth.fetch
	if row[0] == nil
		#puts "nill"
		sth.finish
		sth = $dbh.prepare("SELECT Tags FROM Posts WHERE Id = #{sample_id}")
		sth.execute
		row = sth.fetch
		ans = row[0]
	else
		#puts "not nil"
		#Get parent post
		ans = get_database_tags(row[0])
	end

	# Close the statement handle when done
	sth.finish

	return ans
end


#Finds the tags and puts them into a CSV file
def find_tags(post_ids)


	csv = File.open("#{ARGV[0]}_#{ARGV[1]}_tags.csv", 'w')
	puts "Working..."
	#Process each id
	post_ids.each { |id|

		ids = get_code_ids(id)
		ids.each{ |snippet_id|
			csv.puts("#{id},#{snippet_id},#{get_database_tags(id)}")
		}

	}
	csv.close()
end



#Should have a csv min value, and a max value
begin
	puts Time.new
	connect()

	#get sample
	post_ids = 	get_range_sample(ARGV[0],ARGV[1])

	#Do more work
	find_tags(post_ids)
	puts Time.new
rescue DBI::DatabaseError => e
	puts "An error occurred"
	puts "Error code: #{e.err}"
	puts "Error message: #{e.errstr}"
ensure
	# disconnect from server
	$dbh.disconnect if $dbh
end
