require "dbi"
require "linguist"

module Linguist
	
	#Loads content and finds the linguist tags.  Then saves it to Min_Max_linguist.csv files. 
	class TagsFinder

		#load_map
		def self.load_map(file)
			#Make maps
			$token_map = Hash.new()
			#Read file
			File.open(file, "r") do |f|
				f.each_line do |line|
					array = line.split(",")
					#Only want tuples that are complete 
					if array.length == 2
						#get the database tag
						database_tag = array[1].strip
						if database_tag != ''
							#map database tag to linguist language
							linguist_tag = array[0].strip
							$token_map["#{database_tag}"] = linguist_tag
						end
					end
				end
			end
		end

		#load languages
		def self.load_languages()
			languages = []
			File.open("languages.txt", "r") do |f|
  				f.each_line do |line|

					file = line.strip
					if !($token_map.values.include? file)
						next
					end
					languages << Linguist::Language.find_by_name(file)
				end
			end

			$language_names = languages.map(&:name)

		end

		#Lets linguist read the code
		def self.get_linguist_results(code)
			results = Linguist::Classifier.classify(Samples.cache,code, $language_names).map do |name, _|
				name
			end

			return results
		end

		#Connect to the sql server
		def self.connect()
			# connect to the MySQL server
			$dbh = DBI.connect("DBI:Mysql:sotorrent18_12:localhost", "root", "bokker")
			# get server version string and display it
			row = $dbh.select_one("SELECT VERSION()")
			puts "Server version: " + row[0]
		end 

		#Get sample between the min and max value
		def self.get_range_sample(min,max)

			posts = []
			sth = $dbh.prepare("SELECT DISTINCT Id FROM Posts WHERE Id >= #{min} AND Id <  #{max} AND (PostTypeId = 1 OR PostTypeId = 2)")
			sth.execute
			# Copy the result to the array
			while row=sth.fetch do
				posts << row[0]
			end

			# Close the statement handle when done
			sth.finish

			return posts
		end


		#Get code snippets if there are any
		def self.get_code_snippets(sample_id)

			code = []

			sth = $dbh.prepare("SELECT Id,Content FROM PostBlockVersion WHERE PostHistoryId = (SELECT MAX(PostHistoryId) FROM PostBlockVersion WHERE PostId= #{sample_id}) AND PostBlockTypeId = 2")
			sth.execute
			# Copy the result to the array
			while row=sth.fetch do
				ans = []
				ans << row[0]
				ans << row[1]
				code << ans
			end

			# Close the statement handle when done
			sth.finish

			return code
		end


		#Finds linguist tags
		def self.find_tags(sample_ids)
			
			csv = File.open("#{ARGV[1]}_#{ARGV[2]}_linguist.csv", 'w')

			#Process each id
			sample_ids.each { |id|

				#get snippets if there are any
				code = get_code_snippets(id)

				for index in 0 ... code.size
					csv.puts("#{id}\t#{code[index][0]}\t#{get_linguist_results(code[index][1])}")
				end
			}
			csv.close()
		end

		#Should have a csv file, min, and max value from the command line
		begin
			puts Time.new
			load_map(ARGV[0])
			load_languages()
			connect()

			#get sample
			post_ids = 	get_range_sample(ARGV[1],ARGV[2])

			#Do more work
			find_tags(post_ids)
			puts Time.new
		rescue DBI::DatabaseError => e
			puts "An error occurred"
			puts "Error code: #{e.err}"
			puts "Error message: #{e.errstr}"
		ensure
			# disconnect from server
			$dbh.disconnect if $dbh
		end
	end
end
