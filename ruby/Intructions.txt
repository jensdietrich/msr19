The database connections are hard coded.  You might need to change these.  See the connect method.

==============
LinguistFinder.rb
==============
Used to find the what linguist classifier thinks the content is for all the code snippets.

Needs:
A csv file that maps overflow tags to linguist such as TIOBE.csv
A min tag limit
A max tag limit
Linguist must be installed: gem install github-linguist (used version was v7.1.3)

Output:
A min_max_linguist.csv file (tag separated)
Post Id \t Snippet Id \t Linguist tags

==============
TagGrab.rb
==============
Used to find the database tags for all the code snippets.

Needs:
A min tag limit
A max tag limit

Output:
A min_max_tags.csv file
Post Id, Snippet Id, Database tags