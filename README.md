# README

This repository contains the scripts to reproduce the data presented in the paper [Jens Dietrich, Markus Luczak-Roesch, Elroy Dalefield: Man vs Machine – A Study into language identification of Stackoverflow code snippets. MSR19](https://sites.google.com/site/jensdietrich/publications/preprints/man_vs_machine.pdf).
The data processing was conducted in two steps steps, as described in the paper.

## STEP 1: Dataset Extraction and Pre-Processing

The SOTorrent [1] Snapshot  version 2018-12-09 was downloaded and loaded into a local MySQL database, using the [SOTorrent SQL scripts](https://zenodo.org/record/2273117#.XIwZgRNLjRY).  Snippets were then extracted from the tables and classified using ruby scripts in the `/ruby` folder and the results were stored in two intermediate text files in CSV format. The ruby scripts require the linguist gem to be installed (version `v7.1.3` was used). This can be achieved by running:

`gem install github-linguist`

The database details (URL, username and password) are hardcoded in the respective ruby scripts and may have to be customised in order to reproduce the results, to do this, change the parameters of at the `DBI.connect` callsites in the scripts.

Both ruby scripts can be started with `min` and `max` parameters to retrsict the ranges of database records to be processed. This features was used to manually pararellise and speedup the preprocessing step. 


The intermediate files produced by the ruby scripts have the following format:  

*`UserTags.csv`*:  `post_id`,`snippet_id`,`tag_list` , the tag list has the format `<tag1><tag2>..<tag-k>` , this is the file produced by `ruby/TagGrab.rb`

*`LinguistTags.csv`*:  `post_id \t snippet_id \t tag_list`, the tag list is comma separated,  this is the file produced by `ruby/LinguistFinder.rb`

Those intermediate files are large (3.34 GB and 8.07 GB uncompressed),  and can be downloaded [here](https://my.pcloud.com/publink/show?code=kZnbbl7ZV8cwbe8CnTRwd4b0STH23jku8CKV). Note that the creation of these files took two days on the platform described in the paper.

## STEP 2: Obtaining Results Presented in Section III A: Classification Consistency

The actual data processing that produced the results presented in IIIA was done by scripts written in Java, stored in the `/java` folder respectively. The java scripts can be build with Maven version 3.6.0. They can be executed with Java 8 or better, 16 GB of memory or more is recommended (using `java -Xmx16G`). All scripts terminated in less than five minutes on the platform described in the paper.

`nz.ac.vuw.ecs.msr19.ComputeAlphas` -- Java program to compute the alpha values reported in figure 1, requires the name of the folder with the intermediate data files from step 1 as input.

`nz.ac.vuw.ecs.msr19.ComputeTagCooccurence` -- Java program to compute the tag co-occurence values (format: CSV with three values: `tag1 \t tag2 \t count`), the file produced is used by the R script to produce figure 2. Requires the name of the folder with the intermediate data files from step 1 as input. This file is then further processed to obtain results presented in IIIB as described below.


`nz.ac.vuw.ecs.msr19.ComputeLinguistCooccurence` -- Java program to compute the linguist co-occurence values (format: CSV with three values: `tag1 \t tag2 \t count`), the file produced is used by the R script to produce figure 2. Requires the name of the folder with the intermediate data files from step 1 and a k (for the paper, k=3 was used) as input. This file is then further processed to obtain results presented in IIIB as described below.

## STEP 3: Obtaining Results Presented in Section III B: Tag Co-occurrence


We ran these scripts on R version 3.4.2 using the following platform: x86_64-apple-darwin15.6.0 (64-bit)). The required libraries are: igraph, Rtsne, RColorBrewer, circlize, fpcm and randomcoloR. 

From the extracted user tags as well as computed tags assigned to each code snippet by the linguist library, we constructed two co-occurrence matrices (values represent how often two particular tags co-occur in snippets). In order to determine which languages group well together based on co-occurrence, we apply the t-SNE algorithm (dimensions=2; theta=0.1; perplexity=5) on the co-occurrence matrices, followed by a k-means clustering on the resulting two dimensions. The respectibve scripts can be found in `R/script.R`, the respective parts are separated and further described by comments in this script.

---


*[1] S. Baltes, C. Treude, and S. Diehl. SOTorrent: Studying the origin, evolution, and usage of stack overflow code snippets. In Proceedings MSR’19. IEEE, 2019.*
